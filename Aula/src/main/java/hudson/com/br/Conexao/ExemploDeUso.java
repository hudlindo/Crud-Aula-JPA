package hudson.com.br.Conexao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import hudson.com.br.model.Aluno;
import hudson.com.br.model.Disciplina;

public class ExemploDeUso {



	public static void main(String[] args) {

		// https://www.objectdb.com/java/jpa
		
		//Nesta classe você deve persistir, consultar, atualizar e excluir um aluno e uma disciplina utilizando JPA.

	EntityManagerFactory emf = null;

		try {

			
			emf = Persistence.createEntityManagerFactory("exemplo-jpa-pu");
			EntityManager em = emf.createEntityManager();

			Aluno aluno = new Aluno(1, "hudson",null, "Castelo Branco", 895.6);
			
			System.out.println("persistindo aluno");
			em.getTransaction().begin();
			em.persist(aluno);
			em.getTransaction().commit();
			
		
			System.out.println("procurando aluno");
			aluno = em.find(aluno.getClass(),1);
			System.out.println(aluno.toString());
			
			System.out.println("excluindo aluno");
			em.getTransaction().begin();
			em.remove(aluno);
			em.getTransaction().commit();
			
			Disciplina disciplina = new Disciplina("1", "matemátca", 5);
			
			System.out.println("persistindo aluno");
			em.getTransaction().begin();
			em.persist(disciplina);
			em.getTransaction().commit();
			
		
			System.out.println("procurando aluno");
			disciplina = em.find(disciplina.getClass(),"1");
			System.out.println(disciplina.toString());
			
			System.out.println("excluindo aluno");
			em.getTransaction().begin();
			em.remove(disciplina);
			em.getTransaction().commit();
			
			
			em.close();
			
		} finally {

			if (emf != null) {
				emf.close();
			}

		}

	}
	
//	public static Aluno consultarAluno(int id) {
//		
//		EntityManagerFactory emf = null;
//		emf = Persistence.createEntityManagerFactory("exemplo-jpa-pu");
//		EntityManager em = emf.createEntityManager();
//		
//		//em.getTransaction().commit();
//	
//		return aluno;	
//	}
}
