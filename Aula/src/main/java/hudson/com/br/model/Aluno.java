package hudson.com.br.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Aluno")
public class Aluno {
@Id 		
private Integer matricula ;
private String nome;
private Date dataNascimento;
private String endereco;
private double  rendaFamiliar;


public Aluno() {
	super();
}


public Aluno(Integer matricula, String nome, Date dataNascimento, String endereco, double rendaFamiliar) {
	super();
	this.matricula = matricula;
	this.nome = nome;
	this.dataNascimento = dataNascimento;
	this.endereco = endereco;
	this.rendaFamiliar = rendaFamiliar;
}


public Integer getMatricula() {
	return matricula;
}
public void setMatricula(Integer matricula) {
	this.matricula = matricula;
}
public String getNome() {
	return nome;
}
public void setNome(String nome) {
	this.nome = nome;
}
public Date getDataNascimento() {
	return dataNascimento;
}
public void setDataNascimento(Date dataNascimento) {
	this.dataNascimento = dataNascimento;
}
public String getEndereco() {
	return endereco;
}
public void setEndereco(String endereco) {
	this.endereco = endereco;
}
public double getRendaFamiliar() {
	return rendaFamiliar;
}
public void setRendaFamiliar(double rendaFamiliar) {
	this.rendaFamiliar = rendaFamiliar;
}



@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((dataNascimento == null) ? 0 : dataNascimento.hashCode());
	result = prime * result + ((endereco == null) ? 0 : endereco.hashCode());
	result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
	result = prime * result + ((nome == null) ? 0 : nome.hashCode());
	long temp;
	temp = Double.doubleToLongBits(rendaFamiliar);
	result = prime * result + (int) (temp ^ (temp >>> 32));
	return result;
}





@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Aluno other = (Aluno) obj;
	if (dataNascimento == null) {
		if (other.dataNascimento != null)
			return false;
	} else if (!dataNascimento.equals(other.dataNascimento))
		return false;
	if (endereco == null) {
		if (other.endereco != null)
			return false;
	} else if (!endereco.equals(other.endereco))
		return false;
	if (matricula == null) {
		if (other.matricula != null)
			return false;
	} else if (!matricula.equals(other.matricula))
		return false;
	if (nome == null) {
		if (other.nome != null)
			return false;
	} else if (!nome.equals(other.nome))
		return false;
	if (Double.doubleToLongBits(rendaFamiliar) != Double.doubleToLongBits(other.rendaFamiliar))
		return false;
	return true;
}





@Override
public String toString() {
	return "Aluno [matricula=" + matricula + ", nome=" + nome + ", dataNascimento=" + dataNascimento + ", endereco="
			+ endereco + ", rendaFamiliar=" + rendaFamiliar + "]";
}
}
